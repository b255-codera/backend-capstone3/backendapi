const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require("./routes/userRoutes");
const adminRoutes = require("./routes/adminRoutes");
const productRoutes = require("./routes/productRoutes");
const cartRoutes = require("./routes/cartRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cors = require("cors");
const morgan = require('morgan');
const stripe = require("./routes/stripe")

const app = express();

// Middlewares
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(morgan('dev'));
app.use("/api/users", userRoutes);
app.use("/api/admin", adminRoutes);
app.use("/api/products", productRoutes);
app.use("/api/cart", cartRoutes);
app.use("/api/orders", orderRoutes);
app.use("/api/stripe", stripe)
app.use('/uploads', express.static('./uploads'));

// Connecting to MongoDB database
mongoose.connect("mongodb+srv://jandycodera:admin123@cluster0.my3tqpy.mongodb.net/?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"));

// Server
if(require.main === module) {
	app.listen(process.env.PORT || 4000, () => {
		console.log(`API is now online on port ${ process.env.PORT || 4000}`);
	});
}

module.exports = app;
const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},

	description: {
		type: String,
		required: true
	},

	price: {
		type: Number,
		required: true
	},

	category: {
		type: String,
		required: true
	},

	brand: {
		type: String,
		required: true
	},

	quantity: {
		type: Number,
		required: true
	},

	isActive: {
		type: Boolean,
		default: true
	},

	productImages: [
		{ img: {type: String}}
	],

	createdOn : {
		type: Date,
		default: new Date()
	}

});

module.exports = mongoose.model("Product", productSchema);
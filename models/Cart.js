const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Types.ObjectId,
		ref: "User",
		required: true
	},

	products: [
		{
			productId: {
				type: mongoose.Types.ObjectId,
				ref: "Product",
				required: true
			},

			quantity: {
				type: Number,
				default: 1
			},

			price: {
				type: Number,
				required: true
			},

			subtotal: {
				type: Number
			}
		}
	]

}, { timestamps: true });

module.exports = mongoose.model("Cart", cartSchema);
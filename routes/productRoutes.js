const express = require('express');
const multer = require("multer");
const { createProduct,
		getAllProducts,
		getActiveProducts,
		getLatestProducts,
		getProduct,
		updateProduct,
		archiveProduct,
		activateProduct,
		deleteProduct } 
		= require("../controllers/productController");
const router = express.Router();
const auth = require("../auth");
const shortid = require("shortid");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(path.dirname(__dirname), 'uploads'))
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, shortid.generate() + '-' + file.originalname)
  }
})

const upload = multer({ storage });

// Create a new product
router.post("/create", auth.verify, upload.array('productImage'), (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		createProduct(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});

// Get all products
router.get("/", getAllProducts);

// Get all Active products
router.get("/active", getActiveProducts);

// Retrieve a single product
router.get("/find/:id", getProduct);

// Get all latest products
router.get("/latest", getLatestProducts);

// Update product information
router.put("/:id/update", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		updateProduct(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});

// Archive a product
router.put("/:id/archive", (req, res) => {
		archiveProduct(req, res);
});

// Activate a product
router.put("/:id/activate", (req, res) => {
		activateProduct(req, res);
});


// Delete a product
router.delete("/:id/delete", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		deleteProduct(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});


module.exports = router;
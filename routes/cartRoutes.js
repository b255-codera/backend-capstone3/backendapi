const express = require('express');
const { addProductToCart,
		updateCartQuantity,
		removeProductsFromCart,
		getCart,
		getAllCarts,
		getTotalAmount } 
		= require("../controllers/cartController");
const router = express.Router();
const auth = require("../auth");

// Add products to Cart
router.post("/add-to-cart", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	addProductToCart(req, res, userId);
});

// Update Cart
router.put("/:userId", auth.verify, updateCartQuantity);

// Delete Cart
router.delete("/:userId", auth.verify, removeProductsFromCart);

// Get Cart
router.get("/find/:userId", auth.verify, getCart);

// Get Total Amount
router.get("/:userId/total", auth.verify, getTotalAmount);

module.exports = router;
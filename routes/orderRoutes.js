const express = require('express');
const { createOrder,
		updateOrder,
		cancelOrder,
		getUserOrders,
		getAllOrders,
		updateOrderStatus } 
		= require("../controllers/orderController");
const router = express.Router();
const auth = require("../auth");

// Create Order
router.post("/create", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	createOrder(req, res, userId);
});

// Cancel an Order
router.delete("/:orderId", auth.verify, cancelOrder);

// Get User Orders
router.get("/find/:userId", auth.verify, getUserOrders);

// Get All Orders
router.get("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		getAllOrders(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});

// Update Order Status
router.put("/:orderId/status", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		updateOrderStatus(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});


module.exports = router;
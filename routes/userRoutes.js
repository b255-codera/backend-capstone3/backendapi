const express = require('express');
const { getAllUsers,
		getUser,
		registerUser,
		loginUser,
		updateUser,
		deleteUser,
		changeToAdmin,
		getProfile } 
		= require("../controllers/userController");

const router = express.Router();
const auth = require("../auth");

// Get All Users
router.get("/", (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		getAllUsers(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});

// Get a User
router.get("/find/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		getUser(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});

// Get User Details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	getProfile(req, res, {userId : userData.id});

});

// Signup/Register new user
router.post("/signup", registerUser);

// User Login
router.post("/login", loginUser);

// Update a User
router.put("/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	updateUser(req, res, userData.id);
});

// Delete a user
router.delete("/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		deleteUser(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});

// Change user to Admin
router.post("/:id/admin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true) {
		changeToAdmin(req, res);
	} else {
		res.status(500).json({ 
			message: "You are not authorized to perform this request!"
		});
	}
});

module.exports = router;
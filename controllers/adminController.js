const Admin = require("../models/Admin");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.addAdmin = async (req, res) => {

	const{ email, username, password } = req.body;
	let existingAdmin;
	let admin;
	const hashedPassword = bcrypt.hashSync(password, 10);

	if(!email || !username || !password) 
	{
		return res.status(422).json({
			message: "Please fill out all the fields!"
		});
	}

	try {

		existingAdmin = await Admin.findOne({ email });

		if(existingAdmin) {
			return res.status(400).json({ 
				message: "An admin with the same email already exists!"
			});
		}

		existingAdmin = await Admin.findOne({ username });

		if(existingAdmin) {
			return res.status(400).json({ 
				message: "Username already in use! Please create another one."
			});
		}

		admin = new Admin({ 
			email, 
			username, 
			password:hashedPassword 
		});

		admin = await admin.save();

		return res.status(201).json({
			message: "Success", 
			admin 
		});

	} catch (error) {
		 console.log(error);
		 return res.status(500).json({
		 	message: "An unexpected error occurred."
		 });
	}	
}

module.exports.adminLogin = async (req, res) => {
	const { emailOrUsername, password } = req.body;

	if (!emailOrUsername || !password) {
		return res.status(412).json({ 
			message: "Email/Username and/or password must not be empty" 
		});
	}

	let existingAdmin;
	try {
		// Find admin by email or username
		existingAdmin = await Admin.findOne(
			{
				$or: [
						{ 
							email: emailOrUsername 
						}, 
						{ 
							username: emailOrUsername 
						}
					],
			}
		);

		if (!existingAdmin) {
			return res.status(400).json({ 
				message: "Username/Email or password is incorrect." 
			});
		}

		const isPasswordCorrect = bcrypt.compareSync(password, existingAdmin.password);

		if (!isPasswordCorrect) {
			return res.status(400).json({ 
				message: "Username/Email or password is incorrect." 
			});
		}

		return res.status(200).json({ 
			message: "Success! You are now logged in as an admin.",
			token: auth.createAccessToken(existingAdmin),
			user: existingAdmin
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({ 
			message: "An unexpected error occurred." 
		});
	}
};


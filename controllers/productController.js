const Product = require("../models/Product");
const shortid = require("shortid");

module.exports.createProduct = async (req, res) => {

	// res.status(200).json({file: req.files, body: req.body});
	const { name, description, price, category, brand, quantity } = req.body;

	let productImages = [];
	if(!name || !description || !price || !category || !brand) {
		return res.status(422).json({
			message: "Missing input!"
		});
	}

	if(req.files.length > 0) {
		productImages = req.files.map(file => {
			return {img: file.filename }
		});
	}
	let product;
	try {
		const existingProduct = await Product.findOne({ name, price });

		if (existingProduct) {
			return res.status(409).json({ 
				message: "Duplicate product already exist!" 
			});
		}

		product = new Product({
			name,
			description,
			price,
			category,
			brand,
			quantity,
			productImages
		});

		product = await product.save();
		return res.status(201).json({
			message: "Success!",
			product
		}); 

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.getAllProducts = async (req, res) => {
	
	const queryCategory = req.query.category;
	const queryBrand = req.query.brand;

	try {

		let products;

		if (queryCategory && queryBrand) {
			products = await Product.find({ category: queryCategory, brand: queryBrand});
		} else if (queryCategory && !queryBrand) {
			products = await Product.find({ category: queryCategory });
		} else if (queryBrand && !queryCategory) {
			products = await Product.find({ brand: queryBrand});
		} else {
			products = await Product.find();
		}
		
		
		if(products.length === 0) {
			return res.status(404).json({
				message: "No products found!"
			});
		}

		return res.status(200).json({
			results: products.length,
			products
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


module.exports.getActiveProducts = async (req, res) => {
	
	const queryCategory = req.query.category;
	const queryBrand = req.query.brand;

	try {

		let products;

		if (queryCategory && queryBrand) {
			products = await Product.find({ category: queryCategory, brand: queryBrand, isActive: true });
		} else if (queryCategory && !queryBrand) {
			products = await Product.find({ category: queryCategory, isActive: true });
		} else if (queryBrand && !queryCategory) {
			products = await Product.find({ brand: queryBrand, isActive: true});
		} else {
			products = await Product.find({isActive: true});
		}

		return res.status(200).json({
			results: products.length,
			products
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}
}

module.exports.getLatestProducts = async(req, res) => {
	try {
		const latestProducts = await Product.find().sort({ createdOn: -1 }).limit(8);
		
		if(!latestProducts) {
			return res.status(404).json({
				message: "No products found."
			})
		}

		return res.status(200).json({
			results: latestProducts.length,
			latestProducts
		});
		
	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}
}

module.exports.getProduct = async(req, res) => {

	try {
		const product = await Product.findById(req.params.id);

		if(!product) {
			return res.status(404).json({
				message: "Product not found!"
			});
		}

		return res.status(200).json({
			product
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}
}

module.exports.updateProduct = async (req, res) => {
	const { name, description, price, category, brand } = req.body

	if(!name && !description && !price && !category && !brand) {
		return res.status(422).json({
			message: "Unable to process your request."
		});
	}

	try {
		const updatedProduct = await Product.findByIdAndUpdate(
			req.params.id, 
			{ 
				$set: req.body
			}, 
			{
				new:true
			}
		);

		if(!updatedProduct) {
			return res.status(404).json({
				message: "Product not found!"
			});
		}

		return res.status(200).json({
			message: "The product has been successfully updated.",
			product: updatedProduct
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}
}

module.exports.archiveProduct = async (req, res) => {
	const { isActive } = req.body;

	if(isActive) {
		return res.status(422).json({
			message: "Unable to process your request"
		});
	}

	try {
		const archiveProduct = await Product.findByIdAndUpdate(req.params.id, 
			{ 
				isActive 
			}, 
			{
				new:true
			}
		);

		if(!archiveProduct) {
			return res.status(404).json({
				message: "Product not found!"
			});
		}

		return res.status(200).json({
			message: "Product successfully archived!",
			product: archiveProduct
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.activateProduct = async (req, res) => {
	const { isActive } = req.body;

	if(!isActive) {
		return res.status(422).json({
			message: "Unable to process your request"
		});
	}

	try {
		const archiveProduct = await Product.findByIdAndUpdate(req.params.id, 
			{ 
				isActive 
			}, 
			{
				new:true
			}
		);

		if(!archiveProduct) {
			return res.status(404).json({
				message: "Product not found!"
			});
		}

		return res.status(200).json({
			message: "Product successfully archived!",
			product: archiveProduct
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.deleteProduct = async (req, res) => {
	try {
		const deleteProduct = await Product.findByIdAndRemove(req.params.id);

		if(!deleteProduct) {
			return res.status(404).json({
				message: "Product no longer exist in the database."
			});
		}

		return res.status(200).json({
			message: "Product has been successfully deleted."
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}

}